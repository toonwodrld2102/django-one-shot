from django.forms import ModelForm, DateInput
from todos.models import TodoList, TodoItem


class DateInput(DateInput):
    input_type = "date"


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = [
            "name",
        ]


class TodoItemForm(ModelForm):

    class Meta:
        model = TodoItem
        fields = [
            'task',
            'due_date',
            'is_completed',
            'list'
        ]
        widgets = {
            "due_date": DateInput
        }
